sap.ui.define([
	"com/twobm/toptoygoodsreceived/controller/BaseController",
	"sap/ui/Device",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"./utilities",
	"sap/ui/core/routing/History"
], function (BaseController, Device, MessageBox, MessageToast, Utilities, History) {
	"use strict";

	return BaseController.extend("com.twobm.toptoygoodsreceived.controller.MasterPage", {
		onInit: function () {
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.getRouter().getRoute("MasterPage").attachMatched(this.onRouteMatched, this);
			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.subscribe("OrderApproved", this.onOrderApproved, this);
		},

		onRouteMatched: function (oEvent) {
			// create model to retrieve resaon text in formatter
			this.createGoodsReceivedReasonsModel();
			
			//Select first item if needed.
			this.selectFirstItem();

			// set size limit to ensure list extends 100 
			this.getView().getModel().setSizeLimit(999999999);
		},
		
		onAfterRendering: function() {
			// create model to retrieve resaon text in formatter
			this.createGoodsReceivedReasonsModel();
			
			//Select first item if needed.
			this.selectFirstItem();

			// set size limit to ensure list extends 100 
			this.getView().getModel().setSizeLimit(999999999);
		},

		setObjectListIcon: function (type) {
			return this.getIconFromHeaderType(type);
		},

		onMasterRefresh: function () {
			//Reload all model data - hereby populating all lists in MasterPage
			this.getView().getModel().refresh();
		},

		onOrderApproved: function (a, b, data) {
			//We initialize the Load Model - in order to trigger that first item is selected again (in case it has just been removed)
			this.initializeLoadModel();
		},

		createGoodsReceivedReasonsModel: function () {
			var goodsReceivedReasonsModel = this.getView().getModel("goodsReceivedReasonsModel");
			if (!goodsReceivedReasonsModel) {
				goodsReceivedReasonsModel = new sap.ui.model.json.JSONModel();
				this.getView().setModel(goodsReceivedReasonsModel, "goodsReceivedReasonsModel");
				this.getView().getModel().read("/GoodsReceivePalletReasonSet", {
					success: function (data) {
						goodsReceivedReasonsModel.setData(data.results);
						goodsReceivedReasonsModel.refresh();
						
						// update greve list to ensure formatter for reason text is called
						this.getView().byId("masterListCentral").getBinding("items").refresh(true);
					}.bind(this),
					error: function (error) {
						this.errorCallBackShowInPopUp(error);
					}.bind(this)
				});
			}
		},

		//**********************
		//* Search and filters.
		//**********************
		onSearch: function (oEvent) {
			var doClear = oEvent.getParameter("clearButtonPressed");
			var query = oEvent.getParameter("query");
			if (doClear) {
				//Set initial filters.
				this.applyListFilters();
			} else {
				//Perform Search
				this.applyListFilters(query);
			}
		},

		applyListFilters: function (query) {
			//Prepare default filters for each list.
			var oFilterVendor = new sap.ui.model.Filter("HeaderType", sap.ui.model.FilterOperator.EQ, "VENDOR");
			var oFilterShop = new sap.ui.model.Filter("HeaderType", sap.ui.model.FilterOperator.EQ, "SHOP2SHOP");
			var oFilterCentral = new sap.ui.model.Filter("HeaderType", sap.ui.model.FilterOperator.EQ, "CENTRAL");
			var oFilterAdHoc = new sap.ui.model.Filter("HeaderType", sap.ui.model.FilterOperator.EQ, "ADHOC");
			if (query) {
				//Define search filter.
				var oFilterQuery = new sap.ui.model.Filter("HeaderID", sap.ui.model.FilterOperator.Contains, query);
				//Apply both filters to each list.
				this.getView().byId("masterListVendor").getBinding("items").filter([oFilterVendor, oFilterQuery], sap.ui.model.FilterType.Application);
				this.getView().byId("masterListShop").getBinding("items").filter([oFilterShop, oFilterQuery], sap.ui.model.FilterType.Application);
				this.getView().byId("masterListCentral").getBinding("items").filter([oFilterCentral, oFilterQuery], sap.ui.model.FilterType.Application);
				this.getView().byId("masterListAdHoc").getBinding("items").filter([oFilterAdHoc, oFilterQuery], sap.ui.model.FilterType.Application);
			} else {
				//Apply only default filter to each list.
				this.getView().byId("masterListVendor").getBinding("items").filter([oFilterVendor], sap.ui.model.FilterType.Application);
				this.getView().byId("masterListShop").getBinding("items").filter([oFilterShop], sap.ui.model.FilterType.Application);
				this.getView().byId("masterListCentral").getBinding("items").filter([oFilterCentral], sap.ui.model.FilterType.Application);
				this.getView().byId("masterListAdHoc").getBinding("items").filter([oFilterAdHoc], sap.ui.model.FilterType.Application);
			}
		},

		//*****************
		//* List handlers *
		//*****************
		setParentBusy: function (oEvent, busy) {
			var parent = oEvent.getSource().getParent();
			if (parent) {
				parent.setBusy(busy);
			}
		},

		onVendorListUpdStart: function (oEvent) {
			//Activate busy indicator on the parent Panel control
			this.setParentBusy(oEvent, true);
		},

		onVendorListUpdFinish: function (oEvent) {
			//Deactivate busy indicator on the parent Panel control
			this.setParentBusy(oEvent, false);
			//Update local model with count of List
			var count = oEvent.getSource().getAggregation("items").length;
			this.updateListCountModel("masterListVendor", count);
			this.setLoadStatus("masterListVendor");
		},

		onShopListUpdStart: function (oEvent) {
			//Activate busy indicator on the parent Panel control
			this.setParentBusy(oEvent, true);
		},

		onShopListUpdFinish: function (oEvent) {
			//Deactivate busy indicator on the parent Panel control
			this.setParentBusy(oEvent, false);
			//Update local model with count of List
			var count = oEvent.getSource().getAggregation("items").length;
			this.updateListCountModel("masterListShop", count);
			this.setLoadStatus("masterListShop");
		},

		onCentralListUpdStart: function (oEvent) {
			//Activate busy indicator on the parent Panel control
			this.setParentBusy(oEvent, true);
		},

		onCentralListUpdFinish: function (oEvent) {
			//Deactivate busy indicator on the parent Panel control
			this.setParentBusy(oEvent, false);
			//Update local model with count of List
			var count = oEvent.getSource().getAggregation("items").length;
			this.updateListCountModel("masterListCentral", count);
			this.setLoadStatus("masterListCentral");
		},

		onAdHocListUpdStart: function (oEvent) {
			//Activate busy indicator on the parent Panel control
			this.setParentBusy(oEvent, true);
		},

		onAdHocListUpdFinish: function (oEvent) {
			//Deactivate busy indicator on the parent Panel control
			this.setParentBusy(oEvent, false);
			//Update local model with count of List
			var count = oEvent.getSource().getAggregation("items").length;
			this.updateListCountModel("masterListAdHoc", count);
			this.setLoadStatus("masterListAdHoc");
		},

		createListCountModel: function () {
			var listCountModel = this.getView().getModel("listCountModel");
			if (!listCountModel) {
				//				ListCount Model does not exist - create it.
				if (!listCountModel) {
					listCountModel = new sap.ui.model.json.JSONModel();
					listCountModel.setDefaultBindingMode(sap.ui.model.BindingMode.TwoWay);
				}
				var data = {
					masterListVendor: 0,
					masterListShop: 0,
					masterListCentral: 0,
					masterListAdHoc: 0
				};
				listCountModel.setData(data);
				this.getView().setModel(listCountModel, "listCountModel");
			}
		},

		updateListCountModel: function (parameter, value) {
			var listCountModel = this.getView().getModel("listCountModel");
			if (!listCountModel) {
				this.createListCountModel();
				listCountModel = this.getView().getModel("listCountModel");
			}
			listCountModel.setProperty("/" + parameter, value);
			listCountModel.refresh();
		},

		//************************
		//* First item selection *
		//************************
		selectFirstItem: function () {
			//Attempt to set first item in Vendor List
			if (!this.selectFirstListItem("masterListVendor")) {
				//No item set in Vendor List
				if (!this.selectFirstListItem("masterListShop")) {
					//No item set in Shop2Shop List
					if (!this.selectFirstListItem("masterListCentral")) {
						//No item set in Central List
						this.selectFirstListItem("masterListAdHoc");
					}
				}
			}
		},

		selectFirstListItem: function (list) {
			var oList = this.getView().byId(list);
			var oFirstItem;
			var bItemSet = false;
			for (var key in oList.getItems()) {
				if (key) {
					var oItem = oList.getItems()[key];
					if (oItem.getBindingContextPath()) {
						oFirstItem = oItem;
						break;
					}
				}
			}
			if (oFirstItem) {
				oFirstItem.setSelected(true);
				bItemSet = true;
				//When master and detail are both displayed update detail view with first item.
				if (!Device.system.phone) {
					this.getRouter().navTo("DetailPage", {
						context: oFirstItem.getBindingContextPath().substr(1)
					});
				}
			}
			return bItemSet;
		},

		//*******************
		//* AddHoc Handling *
		//*******************
		onAddAdHoc: function (oEvent) {
			//Create local model for AdHoc Orders (if it does not already exist)
			this.createAdHocModel();
			//Display dialog
			this.AddAdHocDialog = sap.ui.xmlfragment("com.twobm.toptoygoodsreceived.view.fragments.AddAdHocOrderDialog", this);
			this.getView().addDependent(this.AddAdHocDialog);
			this.AddAdHocDialog.open();
		},

		createAdHocModel: function () {
			var adHocModel = this.getView().getModel("AdHocOrderModel");
			if (!adHocModel) {
				//				AdHoc Model does not exist - create it.
				if (!adHocModel) {
					adHocModel = new sap.ui.model.json.JSONModel();
					adHocModel.setDefaultBindingMode(sap.ui.model.BindingMode.TwoWay);
				}
				var data = {
					vendorRefDoc: "",
					newVendorID: ""
				};
				adHocModel.setData(data);
				this.getView().setModel(adHocModel, "AdHocOrderModel");
			}
		},

		onSaveAdHocOrder: function () {
			var adHocModel = this.getView().getModel("AdHocOrderModel");
			var vendorRefDoc = adHocModel.getData().vendorRefDoc;
			var vendorID = adHocModel.getData().newVendorID;
			//Validate inputs.
			if (vendorID === "") {
				MessageToast.show("Vendor ID must be entered");
				return;
			}
			if (vendorID.length > 10) {
				MessageToast.show("Vendor ID must be max 10 characters");
				return;
			}
			if (/[^0-9]/.test(vendorID)) {
				MessageToast.show("Vendor ID contains non-integer value");
				return;
			}
			if (vendorRefDoc === "") {
				MessageToast.show("Vendor Ref. No. must be entered");
				return;
			}

			if (vendorRefDoc.length > 16) {
				MessageToast.show("Max length of Vendor Ref. No. is 16 characters");
				return;
			}
			//Set request for POST in backend
			var request = {
				d: {
					"HeaderType": "ADHOC",
					"VendorRefDoc": vendorRefDoc,
					"VendorID": vendorID
				}
			};
			//Define event handles for POST in backend
			var parameters = {
				success: function (data) {
					//Update local data.
					adHocModel.getData().vendorRefDoc = "";
					adHocModel.getData().newVendorID = "";
					adHocModel.refresh();
					//Refresh model to trigger update of lists
					this.getView().getModel().refresh();
					//Close dialog
					this.AddAdHocDialog.close();
				}.bind(this),
				error: function (error) {
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			};
			//Perform OData call to backend to create AdHoc Header object.
			this.getView().getModel().create("/GoodsReceiveHeaderSet", request, parameters);

		},

		onCancelAdHocOrder: function () {
			this.AddAdHocDialog.close();
		},

		onDeleteAdHocOrder: function (oEvent) {
			var path = oEvent.getParameters().listItem.getBindingContext().sPath;
			//Define event handlers for DELETE in backend
			var parameters = {
				success: function (data) {
					//Refresh model to trigger update of lists
					this.getView().getModel().refresh();
				}.bind(this),
				error: function (error) {
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			};
			//Delete entry in backend.
			this.getView().getModel().remove(path, parameters);
		},

		//*************************************
		//* Load handling of individual lists *
		//*************************************
		initializeLoadModel: function () {
			var loadStatusModel = new sap.ui.model.json.JSONModel();
			loadStatusModel.setDefaultBindingMode(sap.ui.model.BindingMode.TwoWay);
			var data = {
				bCentralLoaded: false,
				bVendorLoaded: false,
				bShopLoaded: false,
				bAdHocLoaded: false
			};
			loadStatusModel.setData(data);
			this.getView().setModel(loadStatusModel, "loadStatusModel");
			return loadStatusModel;
		},

		getLoadModel: function () {
			var loadStatusModel = this.getView().getModel("loadStatusModel");
			if (!loadStatusModel) {
				//Load Status Model does not exist - create it.
				loadStatusModel = this.initializeLoadModel();
			}
			return loadStatusModel;
		},

		setLoadStatus: function (list) {
			var loadStatusModel = this.getLoadModel();
			var bCurrentChanged = false; //This will be set to true only on first load.
			switch (list) {
			case "masterListVendor":
				if (!loadStatusModel.getData().bVendorLoaded) {
					bCurrentChanged = true;
				}
				loadStatusModel.getData().bVendorLoaded = true;
				loadStatusModel.refresh();
				break;
			case "masterListShop":
				if (!loadStatusModel.getData().bShopLoaded) {
					bCurrentChanged = true;
				}
				loadStatusModel.getData().bShopLoaded = true;
				loadStatusModel.refresh();
				break;
			case "masterListCentral":
				if (!loadStatusModel.getData().bCentralLoaded) {
					bCurrentChanged = true;
				}
				loadStatusModel.getData().bCentralLoaded = true;
				loadStatusModel.refresh();
				break;
			case "masterListAdHoc":
				if (!loadStatusModel.getData().bAdHocLoaded) {
					bCurrentChanged = true;
				}
				loadStatusModel.getData().bAdHocLoaded = true;
				loadStatusModel.refresh();
				break;
			}
			if (loadStatusModel.getData().bVendorLoaded && loadStatusModel.getData().bShopLoaded &&
				loadStatusModel.getData().bCentralLoaded && loadStatusModel.getData().bAdHocLoaded &&
				bCurrentChanged) {
				//All lists loaded => display first item.
				this.selectFirstItem();
			}
		},

		//**********************
		//* Selection handling *
		//**********************
		_attachSelectListItemWithContextPath: function (sContextPath) {
			var oView = this.getView();
			var oContent = this.getView().getContent();
			if (oContent) {
				if (!sap.ui.Device.system.phone) {
					var oList = oContent[0].getContent() ? oContent[0].getContent()[0] : undefined;
					if (oList && sContextPath) {
						var sContentName = oList.getMetadata().getName();
						var oItemToSelect, oItem, oContext, aItems, i;
						if (sContentName.indexOf("List") > -1) {
							if (oList.getItems().length) {
								oItemToSelect = null;
								aItems = oList.getItems();
								for (i = 0; i < aItems.length; i++) {
									oItem = aItems[i];
									oContext = oItem.getBindingContext();
									if (oContext && oContext.getPath() === sContextPath) {
										oItemToSelect = oItem;
									}
								}
								if (oItemToSelect) {
									oList.setSelectedItem(oItemToSelect);
								}
							} else {
								oView.addEventDelegate({
									onBeforeShow: function () {
										oList.attachEventOnce("updateFinished", function () {
											oItemToSelect = null;
											aItems = oList.getItems();
											for (i = 0; i < aItems.length; i++) {
												oItem = aItems[i];
												oContext = oItem.getBindingContext();
												if (oContext && oContext.getPath() === sContextPath) {
													oItemToSelect = oItem;
												}
											}
											if (oItemToSelect) {
												oList.setSelectedItem(oItemToSelect);
											}
										});
									}
								});
							}
						}
					} 
				}
			}
		},
		
		formatDeliveryDate: function (deliveryDate) {
			var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "dd/MM/yyyy, HH:mm"
			});
			return dateFormat.format(deliveryDate);
		},

		setPalletReason: function (reasonCode) {
			var result = "";
			var goodsReceivedReasonsModel = this.getView().getModel("goodsReceivedReasonsModel");
			if (goodsReceivedReasonsModel) {
				var reasonItems = this.getView().getModel("goodsReceivedReasonsModel").getData();
				reasonItems.forEach(function (item) {
					if (item.ReasonCode === reasonCode) {
						result = item.Description;
					}
				});
			}
			return result;

		},
		
		setObjectStatusText: function(isPalletsFullyCounted, palletReasonCode) {
			if(isPalletsFullyCounted) {
				return "Pallet Count Completed - Ready To Receive";
			}
			
			var result = "";
			var goodsReceivedReasonsModel = this.getView().getModel("goodsReceivedReasonsModel");
			if (goodsReceivedReasonsModel) {
				var reasonItems = this.getView().getModel("goodsReceivedReasonsModel").getData();
				reasonItems.forEach(function (item) {
					if (item.ReasonCode === palletReasonCode) {
						result = item.Description;
					}
				});
			}
			return result;
		},
		
		setObjectStatusState: function(isPalletsFullyCounted) {
			return isPalletsFullyCounted ? "Success" : "Warning";	
		},
		
		isObjectStatusVisible: function(isPalletsFullyCounted, palletReasonCode) {
			return isPalletsFullyCounted || palletReasonCode !== "" ? true : false;
		},

		_onObjectListItemPress: function (oEvent) {
			var objectListItem = oEvent.getSource().getBindingContext().getObject();
			// show pallet count if Greve and pallet count is not completed
			if (objectListItem.HeaderType === "CENTRAL" && !objectListItem.IsPalletsFullyCounted) {
				this.getRouter().navTo("PalletCount", {
					context: oEvent.getSource().getBindingContext().getPath().substr(1)
				});
			} else {
				this.getRouter().navTo("DetailPage", {
					context: oEvent.getSource().getBindingContext().getPath().substr(1)
				});
			}
		}

	});
}, /* bExport= */ true);