sap.ui.define(["com/twobm/toptoygoodsreceived/controller/BaseController",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"./utilities",
	"sap/ui/core/routing/History"
], function (BaseController, MessageBox, MessageToast, Utilities, History) {
	"use strict";

	return BaseController.extend("com.twobm.toptoygoodsreceived.controller.DetailPage", {
		onInit: function () {
			this.getRouter().getRoute("DetailPage").attachMatched(this.onRouteMatched, this);
		},

		onRouteMatched: function (oEvent) {
			var contextPath = "/" + oEvent.getParameter("arguments").context;
			var givenContext = new sap.ui.model.Context(this.getView().getModel(), contextPath);

			if (!this.getView().getBindingContext() || this.getView().getBindingContext().getPath() !== contextPath) {
				var aExpand = ["Items"];

				this.getView().getModel().createBindingContext(contextPath, "", {
					expand: aExpand.toString()
				}, function () {
					this.getView().setBindingContext(givenContext);
					this.getView().bindElement(contextPath);
					
					this.getView().byId("itemsList").getBinding("items").refresh(true);
					this.getView().getModel().refresh(true);
				}.bind(this), true);
			}

			// clear search field and remove all filters from table
			this.getView().byId("searchField").setValue("");
			this.updateListBinding([]);
		},

		onNavigation: function (oEvent) {
			this.getRouter().navTo("MasterPage", {}, true);
		},

		onListUpdateStarted: function (oEvent) {
			this.getView().setBusy(true);
		},

		onListUpdateFinished: function (oEvent) {
			this.getView().setBusy(false);
		},

		setDetailHeaderIcon: function (type) {
			return this.getIconFromHeaderType(type);
		},

		getItemCount: function (items) {
			var count = 0;
			if (items) {
				count = items.length;
			}
			return "(" + count + ")";
		},

		showAddButton: function (type) {
			return (type === "ADHOC");
		},

		modifyQuantity: function (type) {
			return (type === "VENDOR");
		},

		modifyQuantityNot: function (type) {
			return !this.modifyQuantity(type);
		},

		getDetailListMode: function (type) {
			var result = "None";
			if (type === "ADHOC") {
				result = "Delete";
			}
			return result;
		},
		
		setAcceptText: function(type) {
			return type === "CENTRAL" ? "Receive" : "Approve";
		},

		//****************************
		//* Dialog for Pallet Count. * 
		//****************************
		/*
		showPalletCountDialog: function () {
			this.createPalletCountModel();
			this.setPalletCountSum();
			this.palletCountDialog = sap.ui.xmlfragment("com.twobm.toptoygoodsreceived.view.fragments.PalletCountDialog", this);
			this.getView().addDependent(this.palletCountDialog);

			this.palletCountDialog.open();
		},

		closePalletCountDialog: function () {
			this.palletCountDialog.close();
		},

		setPalletCountSum: function () {
			var path = this.getView().getBindingContext() + "/Pallets";
			var palletCountModel = this.getView().getModel("PalletCountModel");
			var sum = 0;
			var parameters = {
				success: function (data) {
					this.getView().setBusy(false);
					for (var key in data.results) {
						if (data.results[key]) {
							var obj = data.results[key];
							sum = sum + obj.Quantity;
						}
					}
					//Store sum in local model. Note: Must use refresh() to trigger update in screen field.
					palletCountModel.getData().sum = sum;
					palletCountModel.refresh();
				}.bind(this),
				error: function (error) {
					this.getView().setBusy(false);
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			};
			//Read already registered Pallets from backend.
			this.getView().setBusy(true);
			this.getView().getModel().read(path, parameters);
		},

		savePalletCount: function (oEvent) {
			var that = this;
			//Get input value (count) from screen - bound to local model.
			var palletCountModel = this.getView().getModel("PalletCountModel");
			var count = palletCountModel.getData().count;
			if (/[^0-9]/.test(count)) {
				MessageToast.show("Count contains non-integer value");
				return;
			}
			//Prepare request to create entity in backend.
			var headerObj = this.getView().getBindingContext().getObject();
			var request = {
				d: {
					HeaderID: headerObj.HeaderID,
					HeaderType: headerObj.HeaderType,
					Quantity: parseInt(count, 10)
				}
			};
			var parameters = {
				success: function (data) {
					this.getView().setBusy(false);
					MessageToast.show("Saved");
					that.palletCountDialog.close();
				}.bind(this),
				error: function (error) {
					this.getView().setBusy(false);
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			};
			this.getView().setBusy(true);
			this.getView().getModel().create("/GoodsReceivePalletsSet", request, parameters);
		},*/
/*
		createPalletCountModel: function () {
			var palletCountModel = this.getView().getModel("PalletCountModel");
			if (!palletCountModel) {
				palletCountModel = new sap.ui.model.json.JSONModel();
				palletCountModel.setDefaultBindingMode(sap.ui.model.BindingMode.TwoWay);
			}
			var data = {
				count: "",
				sum: 0
			};
			palletCountModel.setData(data);
			this.getView().setModel(palletCountModel, "PalletCountModel");
		},*/

		//**********************************
		//* Dialog for Adding AdHoc Items. * 
		//**********************************
		showAddAdHocItemDialog: function () {
			//Create local model.
			this.createAdHocItemModel();
			//Display Dialog to enter data for a new item object.
			this.AddAdHocItemDialog = sap.ui.xmlfragment("com.twobm.toptoygoodsreceived.view.fragments.AddAdHocItemDialog", this);
			this.getView().addDependent(this.AddAdHocItemDialog);
			this.AddAdHocItemDialog.open();
		},

		createAdHocItemModel: function () {
			var AdHocItemModel = this.getView().getModel("AdHocItemModel");
			if (!AdHocItemModel) {
				AdHocItemModel = new sap.ui.model.json.JSONModel();
				AdHocItemModel.setDefaultBindingMode(sap.ui.model.BindingMode.TwoWay);
			}
			var data = {
				description: "",
				ean: "",
				quantity: null,
				unit: "PC"
			};
			AdHocItemModel.setData(data);
			this.getView().setModel(AdHocItemModel, "AdHocItemModel");
		},

		closeAddAdHocItemDialog: function () {
			this.AddAdHocItemDialog.close();
		},

		onCancelAdHocItem: function () {
			this.closeAddAdHocItemDialog();
		},

		onSaveAdHocItem: function () {
			//Get local binded model to get data entered in dialog.
			var AdHocItemModel = this.getView().getModel("AdHocItemModel");
			var itemDesc = AdHocItemModel.getData().description;
			var itemEAN = AdHocItemModel.getData().ean;
			var itemQuant = AdHocItemModel.getData().quantity;
			var itemUnit = AdHocItemModel.getData().unit;
			//Get header data from BindingContext
			var headerID = this.getView().getBindingContext().getObject().HeaderID;
			var headerType = this.getView().getBindingContext().getObject().HeaderType;
			//Validate input.
			if (itemQuant <= 0) {
				MessageToast.show("Quantity must be greater than zero.");
				return;
			}
			//Set OData request data.
			var request = {
				"HeaderID": headerID,
				"HeaderType": headerType,
				"ItemDesc": itemDesc,
				"MaterialID": itemEAN,
				"Quantity": itemQuant.toString(),
				"UnitCode": itemUnit
			};
			//Define event handles for POST in backend
			var parameters = {
				success: function (data) {
					this.getView().setBusy(false);
					//Reset local data.
					this.createAdHocItemModel();
					AdHocItemModel.refresh();
					//Refresh model to trigger update of list
					this.getView().getModel().refresh();
					//Close dialog
					this.closeAddAdHocItemDialog();
				}.bind(this),
				error: function (error) {
					this.getView().setBusy(false);
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			};
			//Perform OData call to backend to create AdHoc Header object.
			this.getView().setBusy(true);
			this.getView().getModel().create("/GoodsReceiveItemSet", request, parameters);

		},

		onDeleteAdHocItem: function (oEvent) {
			var path = oEvent.getParameters().listItem.getBindingContext().sPath;
			//Define event handlers for DELETE in backend
			var parameters = {
				success: function (data) {
					//Refresh model to trigger update of lists
					this.getView().setBusy(false);
					this.getView().getModel().refresh();
				}.bind(this),
				error: function (error) {
					this.getView().setBusy(false);
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			};
			//Delete entry in backend.
			this.getView().setBusy(true);
			this.getView().getModel().remove(path, parameters);
		},

		//***************************
		//* Generic success Dialog. * 
		//***************************
		/*		showSuccessDialog: function() {
					this.successDialog = sap.ui.xmlfragment("com.twobm.toptoygoodsreceived.view.fragments.SuccessDialog", this);
					this.getView().addDependent(this.successDialog);
					this.successDialog.open();
				},

				setSuccessDialogText: function(type) {
					if (type === "ZUB") {
						return "Please do not forget to adjust stock if necessary";
					} else {
						return "All changes were saved successfully";
					}
				},

				closeSuccessDialog: function() {
					this.successDialog.close();
				},*/

		//****************************
		//* Posting of GoodsReceive. *
		//****************************
		postGoodsReceipt: function (oEvent) {
			var items = this.getView().byId("itemsList").getAggregation("items");
			if (items) {
				if (items.length === 0) {
					MessageToast.show("Add Items first");
					return;
				}
			}
			var object = oEvent.getSource().getBindingContext().getObject();
			if (object.HeaderType === "CENTRAL" || object.HeaderType === "SHOP2SHOP" || object.HeaderType === "ADHOC") {
				//Goods Receive from Central Storage, another shop or AdHoc, is done via a Put/Update on HeaderSet.
				this.doGoodsReceiptHeader(oEvent);
			} else if (object.HeaderType === "VENDOR") {
				//Goods Receive from Vendor is done via Deep Insert where header object and items are passed in a single request.
				this.doGoodsReceiptDeep(oEvent);
			}
		},

		doGoodsReceiptHeader: function (oEvent) {
			//Here we just perform an Update on the header set 
			var path = oEvent.getSource().getBindingContext().sPath;
			var request = {
				d: {
					DoGoodsReceive: true
				}
			};
			
			var headerType = oEvent.getSource().getBindingContext().getObject().HeaderType;
			
			//Define event handlers for Create operation.
			var parameters = {
				success: function (data) {
					//Define event handlers for Remove operation.
					var parametersRem = {
						success: function (dataRem) {
							this.getView().setBusy(false);
							MessageToast.show("Saved");
							this.getView().getModel().refresh();
							this.handleNavToMasterPage(headerType);
						}.bind(this),
						error: function (error) {
							this.getView().setBusy(false);
							this.errorCallBackShowInPopUp(error);
						}.bind(this)
					};
					//Perform remove. Does nothing in backend for CENTRAL and SHOP2SHOP - used only for cleanup in FrontEnd
					//                For ADHOC an actual DELETE will be attempted.
					this.getView().getModel().remove(path, parametersRem);
				}.bind(this),
				error: function (error) {
					this.getView().setBusy(false);
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			};
			//Update in backend
			this.getView().setBusy(true);
			this.getView().getModel().update(path, request, parameters);
		},
		
		handleNavToMasterPage: function(headerType) {
			if(headerType === "CENTRAL"){
				MessageBox.success(
					"Now you need to do your control check by print your count pdf sent to your store email. \n\n Remember to control as minimum the highlighted items. \n\n The documentation must be signed and saved in store.", {
						title: "Posted succesfully",
						onClose: function (sAction) {
							if (sAction === MessageBox.Action.OK) {
								this.getRouter().navTo("MasterPage", {}, true);
							}
						}.bind(this)
					});
					return;
			}
			this.getRouter().navTo("MasterPage", {}, true);
		},

		doGoodsReceiptDeep: function (oEvent) {
			//For this type we need to perform a deep insert (POST) with header and all corresponding items.
			//Get header object and corresponding items.
			var context = oEvent.getSource().getBindingContext();
			var model = this.getView().getModel();
			var hdrObj = context.getObject();
			var itemKeys = model.getProperty(context + "/Items");
			var itemsObj = itemKeys.map(function (item) {
				var itemObj = this.getView().getModel().getProperty("/" + item);
				itemObj.Quantity = itemObj.Quantity.toString();
				return itemObj;
			}.bind(this));
			//Prepare for Post/Create operation.
			var path = "/GoodsReceiveHeaderSet";
			var request = {
				d: {
					HeaderID: hdrObj.HeaderID,
					HeaderType: hdrObj.HeaderType,
					Items: itemsObj
				}
			};
			//Define event handlers for Create operation.
			var parameters = {
				success: function (data) {
					//Define event handlers for Remove operation.
					var parametersRem = {
						success: function (dataRem) {
							this.getView().setBusy(false);
							MessageToast.show("Saved");
							//Notify the MasterPage to have first object correctly selected.
							var oEventBus = sap.ui.getCore().getEventBus();
							oEventBus.publish("OrderApproved");
							this.getView().getModel().refresh();
							this.getRouter().navTo("MasterPage", {}, true);
						}.bind(this),
						error: function (error) {
							this.getView().setBusy(false);
							this.errorCallBackShowInPopUp(error);
						}.bind(this)
					};
					//Perform remove. Does nothing in backend - used only for cleanup in FrontEnd
					this.getView().getModel().remove(context.sPath, parametersRem);
				}.bind(this),
				error: function (error) {
					this.getView().setBusy(false);
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			};

			//Update (via deep insert) in backend
			this.getView().setBusy(true);
			this.getView().getModel().create(path, request, parameters);
		},

		handleSearch: function (oEvent) {
			// create model filter
			var aFilters = [];
			var query = oEvent.getParameter("query");
			if (query && query.length > 0) {
				var filter = new sap.ui.model.Filter("EAN", sap.ui.model.FilterOperator.Contains, query);
				aFilters.push(filter);
			}

			// update list binding
			this.updateListBinding(aFilters);
		},

		updateListBinding: function (aFilters) {
			var list = this.getView().byId("itemsList");
			var binding = list.getBinding("items");
			binding.filter(aFilters);
		},
		
		isEANVisible: function(ean) {
			return ean !== undefined && ean !== "" ? true : false;
		} ,
		isTotalPiecesVisible: function(totalPieces, Quantity) {
			if(totalPieces !== undefined && totalPieces !== "") {
				var intTotalPieces = parseInt(totalPieces, 10);
				var intQuantity = parseInt(Quantity, 10);
				if(intTotalPieces > 0 && intTotalPieces !== intQuantity){
					return true;
				}
			}
			return false;
		}

	});
}, /* bExport= */ true);