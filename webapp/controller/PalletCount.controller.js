sap.ui.define(["com/twobm/toptoygoodsreceived/controller/BaseController",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"./utilities",
	"sap/ui/core/routing/History",
	"sap/ui/core/format/DateFormat"
], function (BaseController, MessageBox, MessageToast, Utilities, History, DateFormat) {
	"use strict";
	return BaseController.extend("com.twobm.toptoygoodsreceived.controller.PalletCount", {
		onInit: function () {
			this.getRouter().getRoute("PalletCount").attachMatched(this.onRouteMatched, this);
		},

		onRouteMatched: function (oEvent) {
			this.getView().setBusy(true);
			this.contextPath = "/" + oEvent.getParameter("arguments").context;
			var givenContext = new sap.ui.model.Context(this.getView().getModel(), this.contextPath);

			if (!this.getView().getBindingContext() || this.getView().getBindingContext().getPath() !== this.contextPath) {
				var aExpand = ["Pallets"];

				this.getView().getModel().createBindingContext(this.contextPath, "", {
					expand: aExpand.toString()
				}, function () {
					this.getView().setBindingContext(givenContext);
					this.getView().bindElement(this.contextPath);
				}.bind(this), true);
			}

			this.createPalletModel(this.contextPath);

			// create client side model based on pallets
			this.setPallets(this.contextPath);
			// create client side model based on pallets
			this.setOrderItems(this.contextPath);
		},

		createPalletModel: function () {
			var palletModel = this.getView().getModel("PalletModel");
			if (!palletModel) {
				palletModel = new sap.ui.model.json.JSONModel();
				palletModel.setDefaultBindingMode(sap.ui.model.BindingMode.TwoWay);
			}

			palletModel.setData({
				Items: [],
				OrderItems: []
			});

			this.getView().setModel(palletModel, "PalletModel");
		},

		setPallets: function (contextPath) {
			this.getView().getModel().read(contextPath + "/Pallets", {
				success: function (data) {
					var items = data.results;
					var palletModel = this.getView().getModel("PalletModel");
					items.forEach(function (item) {
						item.Temp = false;
						palletModel.getData().Items.push(item);
					}.bind(this));
					palletModel.refresh(true);
					this.getView().setBusy(false);
				}.bind(this),
				error: function (error) {
					this.errorCallBackShowInPopUp(error);
					this.getView().setBusy(false);
				}.bind(this)
			});

		},

		setOrderItems: function (contextPath) {
			this.getView().getModel().read(contextPath + "/Items", {
				success: function (data) {
					var items = data.results;
					var palletModel = this.getView().getModel("PalletModel");
					items.forEach(function (item) {
						item.Temp = false;
						palletModel.getData().OrderItems.push(item);
					}.bind(this));
					palletModel.refresh(true);
					this.getView().setBusy(false);
				}.bind(this),
				error: function (error) {
					this.errorCallBackShowInPopUp(error);
					this.getView().setBusy(false);
				}.bind(this)
			});

		},

		openViewOrderDialog: function () {
			var oView = this.getView();
			var viewOrderDialog = oView.byId("viewOrderDialog");

			// create dialog lazily 
			if (!viewOrderDialog) {
				// create dialog via fragment factory 
				viewOrderDialog = sap.ui.xmlfragment(oView.getId(), "com.twobm.toptoygoodsreceived.view.fragments.ViewOrderDialog", this);

				oView.addDependent(viewOrderDialog);
				// clear search field and remove all filters from table in view order dialog
				this.getView().byId("searchFieldViewOrders").setValue("");
				this.updateListBinding([]);
			}
			viewOrderDialog.open();
		},

		closeViewOrderDialog: function () {
			var oView = this.getView();
			var viewOrderDialog = oView.byId("viewOrderDialog");
			viewOrderDialog.close();
		},

		handleSearchInViewOrderDialog: function (oEvent) {
			// create model filter
			var aFilters = [];
			var query = oEvent.getParameter("query");
			if (query && query.length > 0) {
				var filter = new sap.ui.model.Filter("EAN", sap.ui.model.FilterOperator.Contains, query);
				aFilters.push(filter);
			}

			// update list binding
			this.updateListBinding(aFilters);
		},

		updateListBinding: function (aFilters) {
			var list = this.getView().byId("viewOrderItemsList");
			var binding = list.getBinding("items");
			binding.filter(aFilters);
		},

		isTotalPiecesVisible: function (totalPieces, Quantity) {
			if (totalPieces !== undefined && totalPieces !== "") {
				var intTotalPieces = parseInt(totalPieces, 10);
				var intQuantity = parseInt(Quantity, 10);
				if (intTotalPieces > 0 && intTotalPieces !== intQuantity) {
					return true;
				}
			}
			return false;
		},

		isEANVisible: function (ean) {
			return ean !== undefined && ean !== "" ? true : false;
		},

		openAddPalletCountDialog: function () {
			if (!this.addPalletCountDialog) {
				this.addPalletCountDialog = sap.ui.xmlfragment("com.twobm.toptoygoodsreceived.view.fragments.PalletCountDialog", this);
				this.getView().addDependent(this.addPalletCountDialog);
			}

			var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "dd/MM/yyyy, HH:mm"
			});

			var palletModel = this.getView().getModel("PalletModel");
			palletModel.getData().NewItem = {
				Quantity: 1,
				CreatedAt: dateFormat.format(new Date()),
				Temp: true
			};
			palletModel.refresh(true);
			this.getView().setModel(palletModel, "viewModel");
			this.addPalletCountDialog.open();
		},

		closeAddPalletDialogCountDialog: function () {
			this.addPalletCountDialog.close();
		},

		addPallet: function () {
			var palletModel = this.getView().getModel("PalletModel");
			palletModel.getData().Items.push(palletModel.getData().NewItem);
			palletModel.refresh(true);

			this.closeAddPalletDialogCountDialog();
		},

		deletePalletCount: function (oEvent) {
			// remove item from list
			var palletModel = this.getView().getModel("PalletModel");
			var object = oEvent.getSource().getParent().getBindingContext("PalletModel").getObject();
			var array = palletModel.getData().Items;
			array.splice(array.indexOf(object), 1);

			// update counted pallets
			palletModel.getData().CountedPallets = palletModel.getData().CountedPallets - parseInt(object.Count, 10);
			palletModel.refresh(true);
		},

		unCommitedCounts: function () {
			var palletsCounted = false;
			this.getView().getModel("PalletModel").getData().Items.forEach(function (item) {
				if (item.Temp) {
					palletsCounted = true;
				}
			}.bind(this));

			return palletsCounted;
		},

		onNavigation: function () {
			if (this.unCommitedCounts()) {
				MessageBox.warning(
					"Do you wish to go back and delete newly added counts?", {
						actions: [sap.m.MessageBox.Action.OK, sap.m.MessageBox.Action.CANCEL],
						title: "Count Not Approved",
						onClose: function (sAction) {
							if (sAction === MessageBox.Action.OK) {
								this.navToMasterPage();
							}
						}.bind(this)
					});
				return;
			}

			this.navToMasterPage();
		},

		navToMasterPage: function () {
			this.getRouter().navTo("MasterPage", {}, true);
		},

		initiatePostPallet: function () {
			// check if newly added items (temp)
			var palletsCounted = false;
			this.getView().getModel("PalletModel").getData().Items.forEach(function (item) {
				if (item.Temp) {
					palletsCounted = true;
				}
			}.bind(this));

			// check if new counts added or counted pallets are not equal to expected pallets
			var countedPallets = parseInt(this.countCountedPallets(this.getView().getModel("PalletModel").getData().Items), 10);
			var expectedPallets = parseInt(this.getView().getModel().getProperty(this.getView().getBindingContext().getPath() + "/PalletCount"),
				10);
			if (!palletsCounted || countedPallets !== expectedPallets) {
				this.openPalletMiscountingDialog();
				return;
			}

			this.getView().setBusy(true);
			this.postPallets().then(function () {
				this.getView().setBusy(false);
				//Notify the MasterPage to refresh have first object correctly selected.
				var oEventBus = sap.ui.getCore().getEventBus();
				oEventBus.publish("OrderApproved");
				this.getView().getModel().refresh();
				// show success message
				MessageBox.success(
					"Pallet Count is completed. \n\n Next time you open the order you will proceed to goods received.", {
						title: "Posted succesfully",
						onClose: function (sAction) {
							if (sAction === MessageBox.Action.OK) {
								this.navToMasterPage();
							}
						}.bind(this)
					});
			}.bind(this), function (error) {
				this.errorCallBackShowInPopUp(error);
			}.bind(this));
		},

		openPalletMiscountingDialog: function () {
			var oView = this.getView();
			var miscountingDialog = oView.byId("miscountingDialog");
			// create dialog lazily
			if (!miscountingDialog) {
				// create dialog via fragment factory
				miscountingDialog = sap.ui.xmlfragment(oView.getId(),
					"com.twobm.toptoygoodsreceived.view.fragments.PalletMiscountingDialog",
					this);
				oView.addDependent(miscountingDialog);
			}

			// set orderID
			var palletModel = this.getView().getModel("PalletModel");
			var bindingContext = this.getView().getBindingContext();
			var path = bindingContext.getPath();
			var object = bindingContext.getModel().getProperty(path);
			palletModel.getData().Order = object.HeaderID;

			// Set expected pallets property to be shown in dialog
			palletModel.getData().ExpectedPallets = object.PalletCount;
			palletModel.refresh();

			this.setGoodsReceivePalletReasons().then(function () {
				// clear select box
				this.getView().byId("goodsReceivedReasons").setSelectedKey(null);
				// set model
				this.getView().setModel(palletModel, "viewModel");
				miscountingDialog.open();
			}.bind(this), function (error) {
				this.errorCallBackShowInPopUp(error);
			}.bind(this));

			this.getView().setModel(palletModel, "viewModel");
		},

		cancelPalletMiscounting: function () {
			var miscountingDialog = this.getView().byId("miscountingDialog");
			miscountingDialog.close();
		},

		setGoodsReceivePalletReasons: function () {
			return new Promise(function (resolve, reject) {
				this.getView().getModel().read("/GoodsReceivePalletReasonSet", {
					success: function (data) {
						this.getView().getModel("PalletModel").getData().GoodsReceivePalletReasons = data.results;
						this.getView().getModel("PalletModel").refresh();
						resolve();
					}.bind(this),
					error: function (error) {
						reject(error);
					}.bind(this)
				});
			}.bind(this));
		},

		onWarningOptionSelected: function (oEvent) {
			var key = oEvent.getParameter("selectedItem").getKey();
			var palletModel = this.getView().getModel("PalletModel");
			palletModel.getData().WarningAction = key;
			palletModel.refresh();
		},

		isWarningActionVisible: function (value) {
			return value !== undefined && value !== "" ? true : false;
		},

		formatDate: function (date, temp) {
			if (temp) {
				return date;
			}
			var dateFormat = DateFormat.getDateInstance({
				pattern: "dd/MM/yyyy, HH:mm"
			});
			return dateFormat.format(date, true);
		},

		countCountedPallets: function (items) {
			var count = 0;
			if (items) {
				items.forEach(function (item) {
					count = count + item.Quantity;
				}.bind(this));
			}

			return count;
		},

		postPallets: function () {
			return new Promise(function (resolve, reject) {
				var bindingContext = this.getView().getBindingContext();
				var path = bindingContext.getPath();
				var object = bindingContext.getModel().getProperty(path);

				var items = this.getView().getModel("PalletModel").getData().Items;
				items.forEach(function (item) {
					if (item.Temp) {
						var data = {
							properties: {
								HeaderID: object.HeaderID,
								HeaderType: object.HeaderType,
								Quantity: item.Quantity
							}
						};

						this.getView().getModel().createEntry("/GoodsReceivePalletsSet", data);
					}
				}.bind(this));

				// in case reason is needed but no new pallets are added
				if (!this.getView().getModel().hasPendingChanges()) {
					resolve();
					return;
				}

				this.getView().getModel().submitChanges({
					success: function () {
						resolve();
					}.bind(this),
					errer: function (error) {
						reject(error);
					}.bind(this)
				});
			}.bind(this));
		},

		postMiscountingReasonAndPalletCount: function () {
			var selectedReasonItem = this.getView().byId("goodsReceivedReasons").getSelectedItem();
			if (!selectedReasonItem) {
				// Postpone pallet count
				MessageBox.warning(
					"You must select a reason.", {
						title: "Warning"
					});
				return;
			}

			this.postPallets().then(function () {
				this.postMiscountingReason();
			}.bind(this), function (error) {
				this.errorCallBackShowInPopUp(error);
			}.bind(this));
		},

		postMiscountingReason: function () {
			this.getView().byId("miscountingDialog").setBusy(true);
			// get order object
			var bindingContext = this.getView().getBindingContext();
			var path = bindingContext.getPath();
			var object = bindingContext.getModel().getProperty(path);
			// get selectedReasonObj
			var selectedReasonItem = this.getView().byId("goodsReceivedReasons").getSelectedItem();
			var selectedReasonObj = selectedReasonItem.getBindingContext("viewModel").getObject();

			var data = {
				"HeaderID": object.HeaderID,
				"HeaderType": object.HeaderType,
				"ReasonCode": selectedReasonObj.ReasonCode
			};

			this.getView().getModel().create("/GoodsRcvPalletReasApplSet", data, {
				success: function (data) {
					this.getView().byId("miscountingDialog").setBusy(false);
					
					//Notify the MasterPage to refresh have first object correctly selected.
					var oEventBus = sap.ui.getCore().getEventBus();
					oEventBus.publish("OrderApproved");
					this.getView().getModel().refresh();

					// show "posted succesfully" - BE set IsPalletsFullyCounted to true
					if (selectedReasonObj.CanDoGoodsRcv) {
						MessageBox.success(
							selectedReasonObj.Message, {
								title: "Posted succesfully",
								onClose: function (sAction) {
									if (sAction === MessageBox.Action.OK) {
										this.navToMasterPage();
									}
								}.bind(this)
							});
						return;
					}

					// show "Pallet Count Postponed" - BE set IsPalletsFullyCounted to false
					MessageBox.warning(
						selectedReasonObj.Message, {
							title: "Pallet Count Postponed",
							onClose: function (sAction) {
								if (sAction === MessageBox.Action.OK) {
									this.navToMasterPage();
								}
							}.bind(this)
						});
				}.bind(this),
				error: function (error) {
					this.getView().setBusy(false);
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			});
		}

	});
}, /* bExport= */ true);